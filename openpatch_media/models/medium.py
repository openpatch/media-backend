from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
import uuid


class Medium(Base):
    __tablename__ = gt("medium")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    name = db.Column(db.String(256))
    size = db.Column(db.Integer)
    extension = db.Column(db.String(8))
    member_id = db.Column(db.ForeignKey("%s.id" % gt("member")))
    member = db.relationship("Member", back_populates="media")
