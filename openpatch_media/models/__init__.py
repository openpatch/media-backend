from openpatch_core.database import db
from openpatch_media.models import medium, member
from sqlalchemy import orm

orm.configure_mappers()
