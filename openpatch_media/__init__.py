from flask import Flask, jsonify
from instance.config import app_config
from openpatch_core.database import db
from openpatch_core.schemas import ma
from openpatch_media.api.v1 import api as api_v1


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["MAX_CONTENT_LENGTH"] = 1 * 1024 * 1024
    db.init_app(app)
    ma.init_app(app)

    # register api endpoints
    app.register_blueprint(api_v1, url_prefix="/v1")

    @app.route("/healthcheck", methods=["GET"])
    def healthcheck():
        return jsonify({"msg": "ok"}), 200

    return app
