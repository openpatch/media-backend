from openpatch_core.errors import error_manager as em


def access_not_allowed():
    """
    @apiDefine errors_access_not_allowed
    @apiError (ErrorCode 1) {Integer} code 1
    @apiError (ErrorCode 1) {Integer} status_code 403
    @apiError (ErrorCode 1) {String} message AccessNotAllowed
    """
    return em.make_json_error(
        403, message="Your credentials do not allow access to this resource", code=1
    )


def invalid_json(errors):
    return em.make_json_error(
        400, message="Invalid JSON in request body", details=errors, code=2
    )


def invalid_file():
    return em.make_json_error(400, message="Invalid file", code=3)


def resource_not_found():
    """
    @apiDefine errors_resource_not_found
    @apiError (ErrorCode 110) {String} message ResourceNotFound
    @apiError (ErrorCode 110) {Integer} status_code 404
    @apiError (ErrorCode 110) {Integer} code 110
    """
    return em.make_json_error(404, message="Resource not found", code=110)


def file_type_not_allowed():
    """
    @apiDefine errors_file_type_not_allowed
    @apiError (ErrorCode 112) {String} message FileTypeNotAllowed
    @apiError (ErrorCode 112) {Integer} status_code 400
    @apiError (ErrorCode 112) {Integer} code 112
    """
    return em.make_json_error(
        400, message="File type not allowed. Only images.", code=112
    )
