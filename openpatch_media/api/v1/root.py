import os
import uuid

from flask import jsonify, request, send_from_directory
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims, jwt_required
from openpatch_media.api.v1 import api, errors
from openpatch_media.api.v1.schemas.medium import MEDIA_SCHEMA, MEDIUM_SCHEMA
from openpatch_media.models.medium import Medium
from openpatch_media.models.member import Member
from werkzeug.utils import secure_filename

base_url = "/"

UPLOAD_FOLDER = "media"
ALLOWED_EXTENSIONS = {"png", "jpg", "jpeg", "gif", "webp"}


@api.route(base_url, methods=["GET", "POST"])
def media():
    if request.method == "GET":
        return get_media()
    elif request.method == "POST":
        return post_media()


def get_media():
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)
    media = Medium.query.filter_by(member=member).all()
    return jsonify({"media": MEDIA_SCHEMA.dump(media)})


@jwt_required
def post_media():
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    if not "file" in request.files:
        return errors.invalid_file()

    file = request.files["file"]

    if file.filename == "" or not "." in file.filename:
        return errors.invalid_file()

    extension = file.filename.rsplit(".", 1)[1].lower()

    if file and extension in ALLOWED_EXTENSIONS:
        filename = secure_filename(file.filename)
        medium_id = str(uuid.uuid4())
        path = os.path.join(UPLOAD_FOLDER, f"{medium_id}.{extension}")
        file.save(path)
        size = os.stat(path).st_size

        medium = Medium(
            id=medium_id, name=filename, extension=extension, size=size, member=member
        )
        db.session.add(medium)
        db.session.commit()

        return jsonify({"medium_id": medium_id}), 200

    return errors.file_type_not_allowed()


@jwt_required
@api.route(base_url + "<medium_id>", methods=["GET", "DELETE"])
def medium_by_id(medium_id):
    if request.method == "GET":
        return get_medium_by_id(medium_id)
    elif request.method == "DELETE":
        return delete_medium_by_id(medium_id)


def get_medium_by_id(medium_id):
    jwt_claims = get_jwt_claims()
    medium = Medium.query.get(medium_id)

    if not medium:
        return errors.resource_not_found()

    if not str(medium.member_id) == jwt_claims.get("id"):
        return errors.access_not_allowed()

    return jsonify({"medium": MEDIUM_SCHEMA.dump(medium)})


def delete_medium_by_id(medium_id):
    jwt_claims = get_jwt_claims()
    medium = Medium.query.get(medium_id)

    if not medium:
        return errors.resource_not_found()

    if not str(medium.member_id) == jwt_claims.get("id"):
        return errors.access_not_allowed()

    path = os.path.join(
        "/var/www/app", UPLOAD_FOLDER, f"{medium.id}.{medium.extension}"
    )
    try:
        os.remove(path)
    except FileNotFoundError:
        # if the file is not found everything is good.
        pass

    db.session.delete(medium)
    db.session.commit()

    return jsonify({}), 200


@api.route(base_url + "<medium_id>/serve", methods=["GET"])
def serve_medium(medium_id):
    medium = Medium.query.get(medium_id)

    if not medium:
        return errors.resource_not_found()

    filepath = f"{medium.id}.{medium.extension}"
    return send_from_directory(
        f"/var/www/app/{UPLOAD_FOLDER}", filepath, as_attachment=True
    )
