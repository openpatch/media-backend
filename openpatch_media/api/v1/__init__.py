# isort:skip_file
from flask import Blueprint
from flask_cors import CORS

api = Blueprint("api_v1", __name__, template_folder="templates", static_folder="static")

CORS(api)

from openpatch_media.api.v1 import root
