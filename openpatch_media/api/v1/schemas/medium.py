from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_media.models.medium import Medium


class MediumSchema(ma.ModelSchema):
    class Meta:
        model = Medium

    id = fields.UUID()


MEDIUM_SCHEMA = MediumSchema()
MEDIA_SCHEMA = MediumSchema(many=True)
