from marshmallow import fields
from openpatch_core.schemas import ma
from openpatch_media.models.member import Member


class MemberSchema(ma.ModelSchema):
    class Meta:
        model = Member

    id = fields.UUID()


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
