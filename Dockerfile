FROM registry.gitlab.com/openpatch/flask-microservice-base:v3.3.4

ENV OPENPATCH_SERVICE_NAME=media
ENV OPENPATCH_AUTHENTIFICATION_SERVICE=https://api.openpatch.app/authentification
ENV OPENPATCH_DB=sqlite+pysqlite:///database.db
ENV OPENPATCH_ORIGINS=*

COPY "." "/var/www/app"

RUN pip3 install -r /var/www/app/requirements.txt
