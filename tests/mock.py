import os
import shutil
import uuid
from openpatch_core.database import db
from openpatch_media.api.v1.schemas.medium import MEDIA_SCHEMA
from openpatch_media.api.v1.schemas.member import MEMBERS_SCHEMA

members = [{"id": "749e10b4-9545-40ad-9f8a-8e41f08c817b"}]

media = [
    {
        "id": "0c80a0b4-666a-4ff5-b148-058b37764d6d",
        "name": "alpaka.png",
        "size": 20,
        "extension": "png",
        "member": members[0]["id"],
    },
    {
        "id": "7c80a0b4-666a-4ff5-b148-058b37764d6c",
        "name": "bami.png",
        "size": 20,
        "extension": "png",
        "member": members[0]["id"],
    },
]


def mock():
    result = MEMBERS_SCHEMA.load(members, session=db.session)
    db.session.add_all(result)
    db.session.commit()

    try:
        os.mkdir("media")
    except FileExistsError:
        pass
    result = MEDIA_SCHEMA.load(media, session=db.session)
    for medium in result:
        asset_path = os.path.join("tests", "assets", medium.name)
        medium_path = os.path.join("media", f"{medium.id}.{medium.extension}")
        shutil.copyfile(asset_path, medium_path)
    db.session.add_all(result)
    db.session.commit()
