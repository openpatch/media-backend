import os
import mock
from tests.base_test import BaseTest
from openpatch_media.models.medium import Medium


class TestRoot(BaseTest):
    def test_get_media(self):
        response = self.client.get(f"v1/", headers=self.fake_headers["user"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("media", response.get_json())

    def test_post_media(self):
        data = {"file": (open("tests/assets/logo.png", "rb"), "logo.png")}
        response = self.client.post(
            "v1/",
            data=data,
            content_type="multipart/form-data",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("medium_id", response.get_json())

        medium_id = response.get_json()["medium_id"]
        self.assertTrue(
            os.path.isfile(os.path.join("/var/www/app/media", f"{medium_id}.png"))
        )

    def test_get_medium_by_id(self):
        response = self.client.get(
            f"v1/{mock.media[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"v1/{mock.media[0]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 403)

    def test_delete_medium_by_id(self):
        response = self.client.delete(
            f"v1/{mock.media[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(
            os.path.isfile(
                os.path.join("/var/www/app/media", f"{mock.media[0]['id']}.png")
            )
        )

    def test_serve_medium(self):
        response = self.client.get(f"v1/{mock.media[0]['id']}/serve")

        self.assertEqual(response.status_code, 200)
