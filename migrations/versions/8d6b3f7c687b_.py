"""link member to media

Revision ID: 8d6b3f7c687b
Revises: e2181f416698
Create Date: 2020-03-23 18:21:48.213728

"""
from alembic import op
import sqlalchemy as sa
import openpatch_core


# revision identifiers, used by Alembic.
revision = '8d6b3f7c687b'
down_revision = 'e2181f416698'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('media_member', sa.Column('avatar_id', openpatch_core.database.types.GUID(), nullable=True))
    op.add_column('media_member', sa.Column('full_name', sa.String(length=128), nullable=True))
    op.add_column('media_member', sa.Column('username', sa.String(length=64), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('media_member', 'username')
    op.drop_column('media_member', 'full_name')
    op.drop_column('media_member', 'avatar_id')
    # ### end Alembic commands ###
